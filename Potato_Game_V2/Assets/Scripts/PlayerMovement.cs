﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
	private Rigidbody2D rigidbody;
	private bool canJump = true;
	private int score = 0;
	private bool alive = true;

	[SerializeField]
	private int jumpForce = 0;

	[SerializeField]
	private float movementSpeed = 0;

	[SerializeField]
	private Transform camera;

	[SerializeField]
	private Transform background;

	[SerializeField]
	private float backgroundMovementFactor;

	[SerializeField]
	private Temperature temperatureScript;

	[SerializeField]
	private float fps;

	[SerializeField]
	private GameObject userDataPanel;

	[SerializeField]
	private InputField inputField;

	[SerializeField]
	private Text scoreText;

	[SerializeField]
	private Sprite[] sprites;

	private int animationState = 0;

	void Start()
	{
		rigidbody = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate()
	{
		alive = !temperatureScript.gameOver();

		if (Input.GetKeyDown(KeyCode.Escape))
		{
    		Application.LoadLevel(0);
		}

		if (!alive)
		{
			//PlayerPrefs.DeleteAll();
			string username = PlayerPrefs.GetString("username","null");
			if (username == "null")
			{
				userDataPanel.SetActive(true);
			}
			else
			{
				StartCoroutine(insertScore(username, score));
				userDataPanel.SetActive(false);
				Application.LoadLevel(0);
			}
			return;
		}

		if (Input.GetMouseButtonDown(0) && canJump)
		{
			rigidbody.velocity = Vector3.zero;
			rigidbody.AddForce(Vector2.up * jumpForce);
			canJump = false;
			animationState = sprites.Length - 1;
			StartCoroutine(jumpAnimation());
		}
		
		if (canJump)
		{
			rigidbody.AddForce(Vector2.down * 100);
		}

		transform.position += Vector3.right * movementSpeed;
		//background.Translate(Vector2.right * movementSpeed * backgroundMovementFactor);
		background.transform.position = new Vector2(transform.position.x - (score / 30), transform.position.y);

		background.position = new Vector2(background.position.x, transform.position.y);

		camera.position = new Vector3(transform.position.x, transform.position.y, -400);

		if (movementSpeed < 6.0)
		{
			movementSpeed += movementSpeed / 10000;
		}

		score++;
		scoreText.text = "Points: " + score;
	}

	public void sendData()
	{
		string savedName = PlayerPrefs.GetString("username", "null");
		if(savedName == "null")
		{
			savedName = inputField.text;
			StartCoroutine(isUserAvailable(savedName));
		}
		else
		{
			StartCoroutine(insertScore(savedName, score));
			//Application.LoadLevel(0);
		}
	}

	private IEnumerator isUserAvailable(string name)
	{
		WWW www = new WWW("http://students.info.uaic.ro/~emanuel.rusu/FIICode2019/FIICode2019.php?isUserAvailable=" + name + "&key=FIICode20193gw45y6w43a4effg54");
		yield return www;
		if(www.text.Contains("1") == true)
		{
			userDataPanel.transform.GetChild(4).GetComponent<Text>().text = "Username already used!";
		}
		else
		{
			StartCoroutine(insertUser(name, score));			
		}
	}

	private IEnumerator insertScore(string name, int score)
	{
        WWW www = new WWW("http://students.info.uaic.ro/~emanuel.rusu/FIICode2019/FIICode2019.php?user=" + name + "&score=" + score + "&key=FIICode20193gw45y6w43a4effg54");
        yield return www;
        Application.LoadLevel(0);
    }

    private IEnumerator insertUser(string name, int score) 
	{
		WWW www = new WWW("http://students.info.uaic.ro/~emanuel.rusu/FIICode2019/FIICode2019.php?saveUser=" + name + "&key=FIICode20193gw45y6w43a4effg54");
		StartCoroutine(insertScore(name, score));
		yield return www;

		print("insert user");
		PlayerPrefs.SetString("username", name);

		Application.LoadLevel(0);
	}

	IEnumerator jumpAnimation()
	{
		GetComponent<Image>().sprite = sprites[animationState];

		if (animationState > 0)
		{	
			animationState--;

			if (transform.eulerAngles.z > 40 && transform.eulerAngles.z < 250)
			{

				transform.eulerAngles = new Vector3(0, 0, 0);
			}

			if (transform.eulerAngles.z < 340 && transform.eulerAngles.z > 260)
			{
				transform.eulerAngles = new Vector3(0, 0, 0);
			}

			yield return new WaitForSeconds(1 / fps);
			StartCoroutine(jumpAnimation());
		}
	}

	IEnumerator landingAnimation()
	{
		GetComponent<Image>().sprite = sprites[animationState];

		if (animationState < sprites.Length - 1)
		{	
			animationState++;
			yield return new WaitForSeconds(1 / fps);
			StartCoroutine(landingAnimation());
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.name == "Foreground")
		{
			canJump = true;
			StartCoroutine(landingAnimation());
		}

		if (col.gameObject.name == "Snowflake")
		{
			temperatureScript.changeTemperature(-10);
			Destroy(col.gameObject);
		}

		if (col.gameObject.name == "Fire")
		{
			temperatureScript.changeTemperature(10);
			Destroy(col.gameObject);
		}

		if (col.gameObject.name == "Water")
		{
			temperatureScript.dead();
			col.gameObject.GetComponent<BoxCollider2D>().enabled = false;
			Destroy(col.gameObject);
		}
	}
}
