﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class Temperature : MonoBehaviour
{
	private int temperatureLevel = 0;

	[SerializeField]
	private Image termometerObject;

	[SerializeField]
	private int temperatureMaxLevel; // 72 - 109

	[SerializeField]
	private int increaseTemperatureTime;

	private float maxTempHeight = 75;
	private float unitTempHeight;
	private float maxTempWidth;

	private Vector2 termometerObjectPosition;

	void Start()
	{
		unitTempHeight = maxTempHeight / temperatureMaxLevel;
		maxTempWidth = termometerObject.sprite.rect.width;

		termometerObjectPosition = termometerObject.GetComponent<RectTransform>().localPosition;

		termometerObject.GetComponent<RectTransform>().localPosition = new Vector2(termometerObjectPosition.x, termometerObjectPosition.y);

		StartCoroutine(increaseTemperature());
	}

    public void changeTemperature(int temp)
    {
		temperatureLevel += temp;

		if (temperatureLevel < 0)
		{
			temperatureLevel = 0;
		}
		
		termometerObject.GetComponent<RectTransform>().sizeDelta = new Vector2(maxTempWidth, unitTempHeight * temperatureLevel);
		termometerObject.GetComponent<RectTransform>().localPosition = new Vector2(termometerObjectPosition.x, termometerObjectPosition.y + (unitTempHeight * temperatureLevel) / 2);
    }

    public bool gameOver()
    {
    	if (temperatureLevel < temperatureMaxLevel)
    	{
    		return false;
    	}
    	return true;
    }

    public void dead()
    {
    	temperatureLevel = temperatureMaxLevel;
    }

    private IEnumerator increaseTemperature()
    {
    	if (temperatureLevel < temperatureMaxLevel)
    	{
			temperatureLevel++;
			termometerObject.GetComponent<RectTransform>().sizeDelta = new Vector2(maxTempWidth, unitTempHeight * temperatureLevel);
			termometerObject.GetComponent<RectTransform>().localPosition = new Vector2(termometerObjectPosition.x, termometerObjectPosition.y + (unitTempHeight * temperatureLevel) / 2);
    	}
    	else
    	{
    		print("Game Over!");
    	}
		print(temperatureLevel);
    	
    	yield return new WaitForSeconds(increaseTemperatureTime);
    	StartCoroutine(increaseTemperature());
    }
}
