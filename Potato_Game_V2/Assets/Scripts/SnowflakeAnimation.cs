﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SnowflakeAnimation : MonoBehaviour
{
    [SerializeField]
    private Sprite[] sprites;

    [SerializeField]
    private float fps = 30;

    private int spriteID = 0;
    private int sense = 1;

    void Start()
    {
        StartCoroutine(changeFrame());
    }

    IEnumerator changeFrame()
    {
    	if (spriteID < sprites.Length && spriteID >= 0)
    	{
    		GetComponent<Image>().sprite = sprites[spriteID];
    		spriteID += sense;
    	}
    	else
    	{
    		sense = 0 - sense;
    		spriteID += sense;
    	}
    	yield return new WaitForSeconds(1 / fps);
        StartCoroutine(changeFrame());
    }
}
