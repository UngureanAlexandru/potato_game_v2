﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadDestroyer : MonoBehaviour
{
	private int timeToDestroy = 60;

    void Start()
    {
 		StartCoroutine(DestroyRoad());
    }

    IEnumerator DestroyRoad()
    {
    	yield return new WaitForSeconds(timeToDestroy);
    	Destroy(this.gameObject);
    }
}
