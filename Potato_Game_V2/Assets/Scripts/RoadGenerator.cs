﻿// E mult cod care se repeta. O sa il refac atunci cand o sa am timp.
// Momentan, il las asa

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadGenerator : MonoBehaviour
{
	[SerializeField]
	private GameObject[] roadSprites;

	[SerializeField]
	private GameObject canvas;

    [SerializeField]
    private GameObject snowflake;

    [SerializeField]
    private GameObject fire;

    [SerializeField]
    private int snowflakeHeightOffset;

    [SerializeField]
    private int roadGeneratorTime;

    [SerializeField]
    private GameObject player;

    private int roadID = 0;
    private int x = -500;
    private int y = 200;

    private int random;

    void Start()
    {
    	StartCoroutine(generateRoad());
    }

    private IEnumerator generateRoad()
    {
        if (Vector2.Distance(new Vector2(x, y), player.transform.position) < 1000)
        {
            if (roadID == 0)
            {
                S1();
                roadID = Random.Range(0, 4);
            }
            else if (roadID == 1)
            {
                S2();
                roadID = Random.Range(0, 4);
            }
            else if (roadID == 2)
            {
                S3();
                roadID = Random.Range(0, 4);
            }
            else if (roadID == 3)
            {
                S4();
                roadID = Random.Range(4, 6);
            }
            else if (roadID == 4)
            {
                S5();
                roadID = Random.Range(3, 6);
            }
            else if (roadID == 5)
            {
                S6();
                roadID = Random.Range(0, 4);
            }
        }

        yield return new WaitForSeconds(roadGeneratorTime);

        StartCoroutine(generateRoad());
    }

    private void S1()
    {
        x += 500;
        GameObject obj1 = Instantiate(roadSprites[0], new Vector2(x, y), Quaternion.identity);
        obj1.AddComponent<RoadDestroyer>();
        obj1.transform.parent = canvas.transform;
        obj1.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake0 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake0.transform.parent = canvas.transform;
            snowflake0.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }

        y -= 200;
        GameObject obj2 = Instantiate(roadSprites[1], new Vector2(x, y), Quaternion.identity);
        obj2.AddComponent<RoadDestroyer>();
        obj2.transform.parent = canvas.transform;
        obj2.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake1 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake1.transform.parent = canvas.transform;
            snowflake1.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire1 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire1.transform.parent = canvas.transform;
            fire1.name = "Fire";
        }

        x += 500;
        GameObject obj3 = Instantiate(roadSprites[2], new Vector2(x, y), Quaternion.identity);
        obj3.AddComponent<RoadDestroyer>();
        obj3.transform.parent = canvas.transform;
        obj3.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake2 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake2.transform.parent = canvas.transform;
            snowflake2.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire1 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire1.transform.parent = canvas.transform;
            fire1.name = "Fire";
        }
    }

    private void S2()
    {
        x += 500;
        GameObject obj4 = Instantiate(roadSprites[3], new Vector2(x, y), Quaternion.identity);
        obj4.AddComponent<RoadDestroyer>();
        obj4.transform.parent = canvas.transform;
        obj4.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake0 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake0.transform.parent = canvas.transform;
            snowflake0.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }

        y -= 200;
        GameObject obj5 = Instantiate(roadSprites[4], new Vector2(x, y), Quaternion.identity);
        obj5.AddComponent<RoadDestroyer>();
        obj5.transform.parent = canvas.transform;
        obj5.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake1 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake1.transform.parent = canvas.transform;
            snowflake1.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }
    }

    private void S3()
    {
        x += 500;
        GameObject obj6 = Instantiate(roadSprites[5], new Vector2(x, y), Quaternion.identity);
        obj6.AddComponent<RoadDestroyer>();
        obj6.transform.parent = canvas.transform;
        obj6.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake0 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake0.transform.parent = canvas.transform;
            snowflake0.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }

        y -= 200;
        GameObject obj7 = Instantiate(roadSprites[6], new Vector2(x, y), Quaternion.identity);
        obj7.AddComponent<RoadDestroyer>();
        obj7.transform.parent = canvas.transform;
        obj7.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake1 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake1.transform.parent = canvas.transform;
            snowflake1.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }

        x += 500;
        GameObject obj8 = Instantiate(roadSprites[7], new Vector2(x, y), Quaternion.identity);
        obj8.AddComponent<RoadDestroyer>();
        obj8.transform.parent = canvas.transform;
        obj8.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake2 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake2.transform.parent = canvas.transform;
            snowflake2.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }
    }

    private void S4()
    {
        x += 500;
        GameObject obj9 = Instantiate(roadSprites[8], new Vector2(x, y), Quaternion.identity);
        obj9.AddComponent<RoadDestroyer>();
        obj9.transform.parent = canvas.transform;
        obj9.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake0 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake0.transform.parent = canvas.transform;
            snowflake0.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }

        x += 500;
        GameObject obj10 = Instantiate(roadSprites[9], new Vector2(x, y), Quaternion.identity);
        obj10.AddComponent<RoadDestroyer>();
        obj10.transform.parent = canvas.transform;
        obj10.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake1 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake1.transform.parent = canvas.transform;
            snowflake1.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }
    }

    private void S5()
    {
        x += 500;
        GameObject obj11 = Instantiate(roadSprites[10], new Vector2(x, y), Quaternion.identity);
        obj11.AddComponent<RoadDestroyer>();
        obj11.transform.parent = canvas.transform;
        obj11.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake0 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake0.transform.parent = canvas.transform;
            snowflake0.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }

        x += 500;
        GameObject obj12 = Instantiate(roadSprites[11], new Vector2(x, y), Quaternion.identity);
        obj12.AddComponent<RoadDestroyer>();
        obj12.transform.parent = canvas.transform;
        obj12.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake1 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake1.transform.parent = canvas.transform;
            snowflake1.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }
    }

    private void S6()
    {
        x += 500;
        GameObject obj13 = Instantiate(roadSprites[12], new Vector2(x, y), Quaternion.identity);
        obj13.AddComponent<RoadDestroyer>();
        obj13.transform.parent = canvas.transform;
        obj13.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake0 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake0.transform.parent = canvas.transform;
            snowflake0.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }

        x += 500;
        GameObject obj14 = Instantiate(roadSprites[13], new Vector2(x, y), Quaternion.identity);
        obj14.AddComponent<RoadDestroyer>();
        obj14.transform.parent = canvas.transform;
        obj14.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake1 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake1.transform.parent = canvas.transform;
            snowflake1.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }

        y -= 200;
        x -= 500;
        GameObject obj15 = Instantiate(roadSprites[14], new Vector2(x, y), Quaternion.identity);
        obj15.AddComponent<RoadDestroyer>();
        obj15.transform.parent = canvas.transform;
        obj15.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake2 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake2.transform.parent = canvas.transform;
            snowflake2.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }

        x += 500;
        GameObject obj16 = Instantiate(roadSprites[15], new Vector2(x, y), Quaternion.identity);
        obj16.AddComponent<RoadDestroyer>();
        obj16.transform.parent = canvas.transform;
        obj16.name = "Foreground";

        random = Random.Range(0, 3);
        if (random == 1)
        {
            GameObject snowflake3 = Instantiate(snowflake, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            snowflake3.transform.parent = canvas.transform;
            snowflake3.name = "Snowflake";
        }
        else if (random == 2)
        {
            GameObject fire0 = Instantiate(fire, new Vector2(x, y + snowflakeHeightOffset), Quaternion.identity);
            fire0.transform.parent = canvas.transform;
            fire0.name = "Fire";
        }
    }
}
