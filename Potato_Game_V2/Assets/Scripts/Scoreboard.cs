﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scoreboard : MonoBehaviour {

	private List<GameObject> players;
	private List<GameObject> scores;
	public GameObject top1, score1;
	public GameObject top2, score2;
	public GameObject top3, score3;
	public GameObject top4, score4;
	public GameObject top5, score5;

	public GameObject youText;
	// Use this for initialization
	void Start () {
		players = new List<GameObject>();
		scores = new List<GameObject>();
		resetScoreboard();
		initList();
		StartCoroutine(getScoreboard());
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) 
    		SceneManager.LoadScene("manu");
	}
	private void resetScoreboard()
	{
		top1.GetComponent<Text>().text = "";
		top2.GetComponent<Text>().text = "";
		top3.GetComponent<Text>().text = "";
		top4.GetComponent<Text>().text = "";
		top5.GetComponent<Text>().text = "";

		score1.GetComponent<Text>().text = "";
		score2.GetComponent<Text>().text = "";
		score3.GetComponent<Text>().text = "";
		score4.GetComponent<Text>().text = "";
		score5.GetComponent<Text>().text = "";
	}
	private void initList()
	{
		players.Add(top1);
		players.Add(top2);
		players.Add(top3);
		players.Add(top4);
		players.Add(top5);

		scores.Add(score1);
		scores.Add(score2);
		scores.Add(score3);
		scores.Add(score4);
		scores.Add(score5);
	}
	IEnumerator getScoreboard() {
		WWW www = new WWW("http://students.info.uaic.ro/~emanuel.rusu/FIICode2019/FIICode2019.php?getScoreboard=true&key=FIICode20193gw45y6w43a4effg54");
		yield return www;
		string[] words = www.text.Split('\n');
		int j = 0;
		for(int i=0;i<words.Length-2;i+=2) {
			players[j].GetComponent<Text>().text = (j+1) + ". " +  words[i];
			scores[j++].GetComponent<Text>().text = words[i+1];		
		}
		StartCoroutine(getMyScore());
	}
	IEnumerator getMyScore() {
		if(PlayerPrefs.GetString("username","null") != "null") {
			WWW www = new WWW("http://students.info.uaic.ro/~emanuel.rusu/FIICode2019/FIICode2019.php?getMyScore=" + PlayerPrefs.GetString("username","null") + "&key=FIICode20193gw45y6w43a4effg54");
			yield return www;
			string[] words = www.text.Split('\n');
			players[players.Count-1].GetComponent<Text>().text = words[0] + ". " + PlayerPrefs.GetString("username","null");
			scores[scores.Count-1].GetComponent<Text>().text = words[1];
		}
		else
			youText.GetComponent<Text>().text = "";
	}


	public void InsertScore(string name, int score)
	{
		StartCoroutine(insertScore(name, score));
	}
	IEnumerator insertScore(string name, int score) {
        WWW www = new WWW("http://students.info.uaic.ro/~emanuel.rusu/FIICode2019/FIICode2019.php?user=" + name + "&score=" + score + "&key=FIICode20193gw45y6w43a4effg54");
        yield return www;
    }

	public void InsertUser(string name)
	{
		StartCoroutine(insertUser(name));
	}
	public IEnumerator insertUser(string name) 
	{
		WWW www = new WWW("http://students.info.uaic.ro/~emanuel.rusu/FIICode2019/FIICode2019.php?saveUser=" + name + "&key=FIICode20193gw45y6w43a4effg54");
		yield return www;
	}

	/*public bool IsUserAvailable(string name)
	{
		bool result = false;
		StartCoroutine(isUserAvailable(name, (returnValue) => {result = returnValue;}));
		return result;
	}
	IEnumerable isUserAvailable(string name, System.Action<bool> callback)
	{
		WWW www = new WWW("http://students.info.uaic.ro/~emanuel.rusu/FIICode2019/FIICode2019.php?isUserAvailable=" + name + "&key=FIICode20193gw45y6w43a4effg54");
		yield return www;
		if(www.text.Contains("1") == true)
			callback(true);
		else
			callback(false);
	}*/
}
