﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Background : MonoBehaviour
{
	[SerializeField]
	private GameObject player;
	private bool canCreate = true;

    void Update()
    {
        if (player.transform.position.x > transform.position.x && canCreate)
        {
        	canCreate = false;
        	GameObject newBackground = Instantiate(this.gameObject, new Vector2(transform.position.x + 1870, transform.position.y), Quaternion.identity);
        	//newBackground.transform.localScale = new Vector3(1, 1, 1);
            newBackground.GetComponent<RectTransform>().localScale = GetComponent<RectTransform>().localScale / 2.116402f;
        	newBackground.transform.parent = this.gameObject.transform;
        }
    }
}
